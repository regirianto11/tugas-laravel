<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <title>Form Pendafataran</title>
  </head>
  <body>
    <form action="/welcome" method="POST">

      <h1>Buat Account Baru</h1>
      <h2>Sign Up Form</h2>
      @csrf
      <label for="firstname">First Name :</label>
      <br />
      <br />
      <input type="text" id="firstname" name="firstname" />
      <br />
      <br />
      <label for="lastname">Last Name :</label>
      <br />
      <br />
      <input type="text" id="lastname" name="lastname" />
      <br />

      <div class="gender">
        <p>Gender :</p>
        <input type="radio" id="male" name="gender" value="male" />
        <label for="male">Male</label><br />
        <input type="radio" id="female" name="gender" value="female" />
        <label for="female">Female</label><br />
        <input type="radio" id="other" name="gender" value="other" />
        <label for="other">Other</label><br /><br />
      </div>

      <div class="nationality">
        <label for="nationality">Nationality</label><br /><br />
        <select name="nationality" id="nationality">
          <option value="indonesian">Indonesian</option>
          <option value="malaysian">Malaysian</option>
          <option value="australian">Australian</option>
        </select>
      </div>

      <div class="language">
        <p>Language Spoken:</p>
        <input
          type="checkbox"
          id="bahasaindonesia"
          name="bahasaindonesia"
          value="bahasaindonesia"
        />
        <label for="bahasaindonesia">Bahasa Indonesia</label><br />

        <input type="checkbox" id="english" name="english" value="english" />
        <label for="english">English</label><br />
        <input
          type="checkbox"
          id="other1"
          name="other1"
          value="otherlanguage"
        />
        <label for="other1">Other</label><br />
      </div>
      <div class="bio">
        <p>Bio:</p>
        <textarea name="bio" id="bio" cols="40" rows="10"></textarea>
      </div>
      <button type="submit" name="submit">Sign Up</button>
    </form>
  </body>
</html>
